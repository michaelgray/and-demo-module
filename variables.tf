variable "do_token" {
  default = ""
}

variable "do_count" {
  default = 1
}

variable "do_image" {
  default = "27493072"
}

variable "do_region" {
  default = "lon1"
}

variable "do_size" {
  default = "512mb"
}

variable "create_load_balancer" {
  default = false
}

variable "name" {
  default = "my-hello-world-droplet"
}
