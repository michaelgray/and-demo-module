provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_droplet" "web" {
  name      = "${var.name}-${count.index}"
  count     = "${var.do_count}"
  user_data = "#!/bin/bash \ndocker run -d --restart=unless-stopped -p 80:80 tutum/hello-world"
  image     = "${var.do_image}"
  region    = "${var.do_region}"
  size      = "${var.do_size}"
}

resource "digitalocean_loadbalancer" "public" {
  count = "${var.create_load_balancer}"

  name   = "${var.name}-loadbalancer"
  region = "${var.do_region}"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 80
    target_protocol = "http"
  }

  healthcheck {
    port     = 80
    protocol = "tcp"
  }

  droplet_ids = ["${digitalocean_droplet.web.*.id}"]
}
