output "web" {
  value = ["${digitalocean_droplet.web.*.ipv4_address}"]
}
